/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';
import './styles/swap_game.css';
import './styles/search-games.css';
import './styles/common.css';
import './styles/games.css';
import './styles/show.css';
import './js/form.js';
import './js/dropdown.js';
import './js/rating.js';
import './styles/user_profil.css';
import './styles/toolkit-all.css';
import './styles/swap_recap.css';
import './styles/rating.css';
import './styles/channel.css';
import './styles/registration.css';
import './styles/popular-games.css';

// start the Stimulus application
//import './bootstrap';
const $ = require('jquery');
const jquery = require('jquery');
const jQuery = require('jquery');
require('jquery');
require('select2');
require('bootstrap');