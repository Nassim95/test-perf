const allStars = document.querySelectorAll(".fa-star");

init();

function init(){
    allStars.forEach(star => {
        star.addEventListener("click", saveRating)
        star.addEventListener("mouseover", addCSS);
        star.addEventListener("mouseleave", removeCSS);
    })
}

function addCSS(e, css="checked"){
    const hoveredStar = e.target;
    hoveredStar.classList.add(css);
    const previousSiblings = getPreviousSiblings(hoveredStar);
    console.log("previousSiblings", previousSiblings);
    previousSiblings.forEach(elem => elem.classList.add(css));
}

function removeCSS(e, css = "checked"){
    const hoveredStar = e.target;
    hoveredStar.classList.remove(css);

    const previousSiblings = getPreviousSiblings(hoveredStar);
    previousSiblings.forEach(elem => elem.classList.remove(css));
}

function saveRating(e){
    removeEventListenerToAllStars();
}

function removeEventListenerToAllStars(){
    allStars.forEach(star => {
        star.removeEventListener("click", saveRating);
        star.removeEventListener("mouseover", addCSS);
        star.removeEventListener("mouseleave", removeCSS);
    });
}

function getPreviousSiblings(elem){
    console.log("elem.previousSibling", elem.previousSibling);
    let siblings = [];
    const spanNodeType = 1;
    while(elem = elem.previousSibling){
        if(elem.nodeType === spanNodeType){
            siblings = [elem, ...siblings];
        }
    }
    return siblings;
}
