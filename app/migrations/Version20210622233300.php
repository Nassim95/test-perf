<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210622233300 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE comment_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE exchange_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE offer_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE reset_password_request_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "user_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE comment (id INT NOT NULL, author_id INT NOT NULL, parent_id INT DEFAULT NULL, content TEXT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_9474526CF675F31B ON comment (author_id)');
        $this->addSql('CREATE INDEX IDX_9474526C727ACA70 ON comment (parent_id)');
        $this->addSql('CREATE TABLE company (id INT NOT NULL, name VARCHAR(255) NOT NULL, country INT DEFAULT NULL, description TEXT DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE company_game (company_id INT NOT NULL, game_id INT NOT NULL, PRIMARY KEY(company_id, game_id))');
        $this->addSql('CREATE INDEX IDX_60462B62979B1AD6 ON company_game (company_id)');
        $this->addSql('CREATE INDEX IDX_60462B62E48FD905 ON company_game (game_id)');
        $this->addSql('CREATE TABLE exchange (id INT NOT NULL, offer_id INT DEFAULT NULL, user_owner_id INT DEFAULT NULL, user_proposer_id INT DEFAULT NULL, game_id INT DEFAULT NULL, owner_game_id INT DEFAULT NULL, confirmed BOOLEAN DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D33BB07953C674EE ON exchange (offer_id)');
        $this->addSql('CREATE INDEX IDX_D33BB0799EB185F9 ON exchange (user_owner_id)');
        $this->addSql('CREATE INDEX IDX_D33BB07918246FA1 ON exchange (user_proposer_id)');
        $this->addSql('CREATE INDEX IDX_D33BB079E48FD905 ON exchange (game_id)');
        $this->addSql('CREATE INDEX IDX_D33BB07968F95FDA ON exchange (owner_game_id)');
        $this->addSql('CREATE TABLE game (id INT NOT NULL, name VARCHAR(255) NOT NULL, first_release_date INT DEFAULT NULL, status VARCHAR(255) DEFAULT NULL, storyline TEXT DEFAULT NULL, summary TEXT DEFAULT NULL, version_title VARCHAR(255) DEFAULT NULL, aggregated_rating DOUBLE PRECISION DEFAULT NULL, aggregated_rating_count INT DEFAULT NULL, follows INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE game_game_mode (game_id INT NOT NULL, game_mode_id INT NOT NULL, PRIMARY KEY(game_id, game_mode_id))');
        $this->addSql('CREATE INDEX IDX_AE79EA85E48FD905 ON game_game_mode (game_id)');
        $this->addSql('CREATE INDEX IDX_AE79EA85E227FA65 ON game_game_mode (game_mode_id)');
        $this->addSql('CREATE TABLE game_platform (game_id INT NOT NULL, platform_id INT NOT NULL, PRIMARY KEY(game_id, platform_id))');
        $this->addSql('CREATE INDEX IDX_92162FEDE48FD905 ON game_platform (game_id)');
        $this->addSql('CREATE INDEX IDX_92162FEDFFE6496F ON game_platform (platform_id)');
        $this->addSql('CREATE TABLE game_mode (id INT NOT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) DEFAULT NULL, url VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE genre (id INT NOT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE offer (id INT NOT NULL, proposer_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_29D6873EB13FA634 ON offer (proposer_id)');
        $this->addSql('CREATE TABLE platform (id INT NOT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) DEFAULT NULL, url VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE reset_password_request (id INT NOT NULL, user_id INT NOT NULL, selector VARCHAR(20) NOT NULL, hashed_token VARCHAR(100) NOT NULL, requested_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, expires_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_7CE748AA76ED395 ON reset_password_request (user_id)');
        $this->addSql('COMMENT ON COLUMN reset_password_request.requested_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN reset_password_request.expires_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, username VARCHAR(180) NOT NULL, email VARCHAR(255) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649F85E0677 ON "user" (username)');
        $this->addSql('CREATE TABLE User_Own_Games (user_id INT NOT NULL, game_id INT NOT NULL, PRIMARY KEY(user_id, game_id))');
        $this->addSql('CREATE INDEX IDX_52A2CB7FA76ED395 ON User_Own_Games (user_id)');
        $this->addSql('CREATE INDEX IDX_52A2CB7FE48FD905 ON User_Own_Games (game_id)');
        $this->addSql('CREATE TABLE User_Wish_Games (user_id INT NOT NULL, game_id INT NOT NULL, PRIMARY KEY(user_id, game_id))');
        $this->addSql('CREATE INDEX IDX_F8BEE731A76ED395 ON User_Wish_Games (user_id)');
        $this->addSql('CREATE INDEX IDX_F8BEE731E48FD905 ON User_Wish_Games (game_id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CF675F31B FOREIGN KEY (author_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C727ACA70 FOREIGN KEY (parent_id) REFERENCES comment (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE company_game ADD CONSTRAINT FK_60462B62979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE company_game ADD CONSTRAINT FK_60462B62E48FD905 FOREIGN KEY (game_id) REFERENCES game (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE exchange ADD CONSTRAINT FK_D33BB07953C674EE FOREIGN KEY (offer_id) REFERENCES offer (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE exchange ADD CONSTRAINT FK_D33BB0799EB185F9 FOREIGN KEY (user_owner_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE exchange ADD CONSTRAINT FK_D33BB07918246FA1 FOREIGN KEY (user_proposer_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE exchange ADD CONSTRAINT FK_D33BB079E48FD905 FOREIGN KEY (game_id) REFERENCES game (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE exchange ADD CONSTRAINT FK_D33BB07968F95FDA FOREIGN KEY (owner_game_id) REFERENCES game (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE game_game_mode ADD CONSTRAINT FK_AE79EA85E48FD905 FOREIGN KEY (game_id) REFERENCES game (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE game_game_mode ADD CONSTRAINT FK_AE79EA85E227FA65 FOREIGN KEY (game_mode_id) REFERENCES game_mode (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE game_platform ADD CONSTRAINT FK_92162FEDE48FD905 FOREIGN KEY (game_id) REFERENCES game (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE game_platform ADD CONSTRAINT FK_92162FEDFFE6496F FOREIGN KEY (platform_id) REFERENCES platform (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE offer ADD CONSTRAINT FK_29D6873EB13FA634 FOREIGN KEY (proposer_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE reset_password_request ADD CONSTRAINT FK_7CE748AA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE User_Own_Games ADD CONSTRAINT FK_52A2CB7FA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE User_Own_Games ADD CONSTRAINT FK_52A2CB7FE48FD905 FOREIGN KEY (game_id) REFERENCES game (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE User_Wish_Games ADD CONSTRAINT FK_F8BEE731A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE User_Wish_Games ADD CONSTRAINT FK_F8BEE731E48FD905 FOREIGN KEY (game_id) REFERENCES game (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE comment DROP CONSTRAINT FK_9474526C727ACA70');
        $this->addSql('ALTER TABLE company_game DROP CONSTRAINT FK_60462B62979B1AD6');
        $this->addSql('ALTER TABLE company_game DROP CONSTRAINT FK_60462B62E48FD905');
        $this->addSql('ALTER TABLE exchange DROP CONSTRAINT FK_D33BB079E48FD905');
        $this->addSql('ALTER TABLE exchange DROP CONSTRAINT FK_D33BB07968F95FDA');
        $this->addSql('ALTER TABLE game_game_mode DROP CONSTRAINT FK_AE79EA85E48FD905');
        $this->addSql('ALTER TABLE game_platform DROP CONSTRAINT FK_92162FEDE48FD905');
        $this->addSql('ALTER TABLE User_Own_Games DROP CONSTRAINT FK_52A2CB7FE48FD905');
        $this->addSql('ALTER TABLE User_Wish_Games DROP CONSTRAINT FK_F8BEE731E48FD905');
        $this->addSql('ALTER TABLE game_game_mode DROP CONSTRAINT FK_AE79EA85E227FA65');
        $this->addSql('ALTER TABLE exchange DROP CONSTRAINT FK_D33BB07953C674EE');
        $this->addSql('ALTER TABLE game_platform DROP CONSTRAINT FK_92162FEDFFE6496F');
        $this->addSql('ALTER TABLE comment DROP CONSTRAINT FK_9474526CF675F31B');
        $this->addSql('ALTER TABLE exchange DROP CONSTRAINT FK_D33BB0799EB185F9');
        $this->addSql('ALTER TABLE exchange DROP CONSTRAINT FK_D33BB07918246FA1');
        $this->addSql('ALTER TABLE offer DROP CONSTRAINT FK_29D6873EB13FA634');
        $this->addSql('ALTER TABLE reset_password_request DROP CONSTRAINT FK_7CE748AA76ED395');
        $this->addSql('ALTER TABLE User_Own_Games DROP CONSTRAINT FK_52A2CB7FA76ED395');
        $this->addSql('ALTER TABLE User_Wish_Games DROP CONSTRAINT FK_F8BEE731A76ED395');
        $this->addSql('DROP SEQUENCE comment_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE exchange_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE offer_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE reset_password_request_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE "user_id_seq" CASCADE');
        $this->addSql('DROP TABLE comment');
        $this->addSql('DROP TABLE company');
        $this->addSql('DROP TABLE company_game');
        $this->addSql('DROP TABLE exchange');
        $this->addSql('DROP TABLE game');
        $this->addSql('DROP TABLE game_game_mode');
        $this->addSql('DROP TABLE game_platform');
        $this->addSql('DROP TABLE game_mode');
        $this->addSql('DROP TABLE genre');
        $this->addSql('DROP TABLE offer');
        $this->addSql('DROP TABLE platform');
        $this->addSql('DROP TABLE reset_password_request');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('DROP TABLE User_Own_Games');
        $this->addSql('DROP TABLE User_Wish_Games');
    }
}
