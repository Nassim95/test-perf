import http from 'k6/http';
import { sleep } from 'k6';

export let options = {
  stages: [
    { duration: '10s', target: 100 }, // below normal load
    { duration: '10s', target: 100 },
    { duration: '10s', target: 200 }, // normal load
    { duration: '10s', target: 200 },
    { duration: '10s', target: 300 }, // around the breaking point
    { duration: '10s', target: 300 },
    { duration: '10s', target: 400 }, // beyond the breaking point
    { duration: '10s', target: 400 },
    { duration: '10s', target: 0 }, // scale down. Recovery stage.
  ],
};

export default function () {
  const BASE_URL = 'http://swapit-esgi.herokuapp.com'; // make sure this is not production

  let responses = http.batch([
    [
      'GET',
      `${BASE_URL}/show/2003`,
      null,
      { tags: { name: 'Batman' } },
    ],
    [
      'GET',
      `${BASE_URL}/show/8258`,
      null,
      { tags: { name: 'Street Fighter V' } },
    ],
    [
      'GET',
      `${BASE_URL}/show/1877`,
      null,
      { tags: { name: 'Cyberpunk 2077' } },
    ],
    [
      'GET',
      `${BASE_URL}/show/19554`,
      null,
      { tags: { name: 'Steep' } },
    ],
  ]);

  sleep(1);
}
