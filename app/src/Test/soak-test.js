import http from 'k6/http';
import { sleep } from 'k6';

export let options = {
  stages: [
    { duration: '30s', target: 400 }, // ramp up to 400 users
    { duration: '35s', target: 400 }, // stay at 400 for ~4 hours
    { duration: '30s', target: 0 }, // scale down. (optional)
  ],
};

const API_BASE_URL = 'http://swapit-esgi.herokuapp.com';

export default function () {
  http.batch([
    ['GET', `${API_BASE_URL}/games`],
    ['GET', `${API_BASE_URL}/games`],
    ['GET', `${API_BASE_URL}/games`],
    ['GET', `${API_BASE_URL}/games`],
  ]);

  sleep(1);
}
