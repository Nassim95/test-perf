<?php

namespace App\Controller\Admin;

use App\Repository\CommentRepository;
use App\Repository\ExchangeRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(UserRepository $user, CommentRepository $comment, ExchangeRepository $exchange): Response
    {
        return $this->render('admin/user/index.html.twig', [
            'users' =>  $user->findAll(),
            'comments'=> $comment->findAll(),
            'exchanges' => $exchange->findAll()
        ]);
    }

}
