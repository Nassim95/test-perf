<?php

namespace App\Controller;

use App\Entity\Game;
use App\Entity\User;
use App\Entity\Rating;
use App\Form\AddGameToListType;
use App\Repository\GameRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\ResetPasswordRequestFormType;
use Symfony\Component\HttpFoundation\Request;
use App\lib\IgdbBundle\IgdbWrapper\IgdbWrapper;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserController extends AbstractController
{
    /**
     * @Route("/user_profile/{user}", name="user_profile")
     */
    public function index(User $user = null, IgdbWrapper $igdb)
    {

        if($user === null){
            $user = $this->getUser();
        }
       
        $ownGames = $user->getOwnGames();
        $wishGames = $user->getWishGames();
        $ratings = $user->getRatings();
        
        $waitingExchanges = [];
        $confirmedExchanges = [];

        foreach($user->getExchanges() as $exchange){
            if($exchange->getConfirmed() === true){
                array_push($confirmedExchanges, $exchange);
            }
            elseif($exchange->getConfirmed() === null){
                array_push($waitingExchanges, $exchange);
            }
        }

        $numberExchanges = count($confirmedExchanges);

        $arrayWishGames = [];
        $arrayOwnGames = [];
        $arrayRatings = [];
        
        foreach ($ownGames as $ownGame) {
            array_push($arrayOwnGames, $ownGame);
        }

        foreach ($wishGames as $wishGame) {
            array_push($arrayWishGames, $wishGame);
        }

        foreach ($ratings as $rating) {
            array_push($arrayRatings, $rating);
        }
        
        if(!empty($arrayRatings)){
            $sum_ratings = 0;

            for($i = 0; $i < count($arrayRatings); $i++){
                $sum_ratings += $arrayRatings[$i]->getRatingValue();
            }
            
            $averageRate = $sum_ratings / count($arrayRatings);
            $totalRatings = count($arrayRatings);
        }else{
            $averageRate = 0;
            $totalRatings = 0;
        }

        return $this->render('front/user/user_profile.html.twig', [
            'controller_name' => 'UserController',
            'user'=>$user,
            'ownGames' => $arrayOwnGames,
            'wishGames' => $arrayWishGames,
            'confirmedExchanges' => $confirmedExchanges,
            'waitingExchanges' => $waitingExchanges,
            'averageRate' => $averageRate,
            'igdb' => $igdb,
            'totalRatings' => $totalRatings,
            'numberExchanges' => $numberExchanges
        ]);
    }


    /**
     * @Route("/quickaddowngame/{game}", name="quickaddowngame")
     */
    public function quickAddOwnGame(Game $game, EntityManagerInterface $em){

        $user = $this->getUser()->addOwnGame($game);
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('user_profile');
    }

    /**
     * @Route("/quickdeleteowngame/{game}", name="quickdeleteowngame")
     */
    public function quickDeleteOwnGame(Game $game, EntityManagerInterface $em){

        $user = $this->getUser()->removeOwnGame($game);
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('user_profile');
    }

    /**
     * @Route("/quickdeleteowngamefromlist/{game}", name="quickdeleteowngamefromlist")
     */
    public function quickDeleteOwnGameFromList(Game $game, EntityManagerInterface $em){

        $user = $this->getUser()->removeOwnGame($game);
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('userowngameslists');
    }

    /**
     * @Route("/quickdeletewishgame/{game}", name="quickdeletewishgame")
     */
    public function quickDeleteWishGame(Game $game, EntityManagerInterface $em){

        $user = $this->getUser()->removeWishGame($game);
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('user_profile');
    }

    /**
     * @Route("/quickdeletewishgamefromlist/{game}", name="quickdeletewishgamefromlist")
     */
    public function quickDeleteWishGameFromList(Game $game, EntityManagerInterface $em){

        $user = $this->getUser()->removeWishGame($game);
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('userwishgameslists');
    }

    /**
     * @Route("/userowngameslists", name="userowngameslists")
     */
    public function userOwnGamesLists(Request $request, IgdbWrapper $igdb){

        $userOwnGames = $this->getUser()->getOwnGames();
        $arrayOwnGames = [];

        $form = $this->createForm(AddGameToListType::class);

        $form->add('OwnGames', EntityType::class, [
                'label' => false,
                'class' => Game::class,
                'multiple' => 'multiple',
                'method' => "post" ,
                'action' => "",
                'attr' => [
                    'class' => 'form-control js-example-basic-multiple',
                    'placeholder' => 'Jeux',
                    'name' => "states[]"
                ],
                'choice_label' => 'name'
        ]);


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->get($form->getName());
            $dataOwnGames = $data['OwnGames'];
            $user = $this->getUser();
            foreach ($dataOwnGames as $ownGame){
                $game = $this->getDoctrine()->getRepository(Game::class)->find($ownGame);
                $user->addOwnGame($game);
            } 
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Jeux ajoutés !');
        }

        foreach ($userOwnGames as $userOwnGame) {
            array_push($arrayOwnGames, $userOwnGame);
        }

        return $this->render('front/user/user_own_games_list.html.twig', [
            'form' => $form->createView(),
            'userOwnGames' => $arrayOwnGames,
            'igdb' => $igdb
        ]);
    }

    /**
     * @Route("/deleteOwnGames/{id}", name="deleteOwnGames")
     */
    public function deleteOwnGames()
    {
        $user = $this->getUser();

        return $this->redirectToRoute('game_index');
    }

    /**
     * @Route("/userwishgameslists", name="userwishgameslists")
     */
    public function userWishGamesLists(Request $request, IgdbWrapper $igdb){

        $userWishGames = $this->getUser()->getWishGames();
        $arrayWishGames = [];

        $form = $this->createForm(AddGameToListType::class);

        $form->add('WishGames', EntityType::class, [
                'label' => false,
                'class' => Game::class,
                'multiple' => 'multiple',
                'method' => "post" ,
                'action' => "",
                'attr' => [
                    'class' => 'form-control js-example-basic-multiple',
                    'placeholder' => 'Jeux',
                    'name' => "states[]"
                ],
                'choice_label' => 'name'
        ]);


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->get($form->getName());
            $dataWishGames = $data['WishGames'];
            $user = $this->getUser();
            foreach ($dataWishGames as $wishGame){
                $game = $this->getDoctrine()->getRepository(Game::class)->find($wishGame);
                $user->addWishGame($game);
            } 
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Jeux ajoutés !');
        }

        foreach ($userWishGames as $userWishGame) {
            array_push($arrayWishGames, $userWishGame);
        }

        return $this->render('front/user/user_wish_games_list.html.twig', [
            'form' => $form->createView(),
            'userWishGames' => $arrayWishGames,
            'igdb' => $igdb
        ]);
    }

    /**
     * @Route("/deleteWishGames", name="deleteWishGames")
     */
    public function deleteWishGames()
    {
        $ownGame = $this->getUser()->getWishGames();

        $user = $this->getUser();
        $game = $this->getDoctrine()->getRepository(Game::class)->find($ownGame);
        $user->removeWishGames($game);

        return $this->redirectToRoute('game_index');
    }

    /**
     * @Route("/rating_user/{user}/{selected_game}/{ownerGame}/{owner}", name="rating_user")
     */
    public function ratingUser(){ 
        return $this->render('front/rating/user_rating.html.twig');
    }
}
