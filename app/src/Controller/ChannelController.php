<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ChannelRepository;
use App\Repository\MessageRepository;
use App\Services\Mercure\CookieJwtProvider;
use App\Entity\Channel;
use App\Entity\User;
use Symfony\Component\Serializer\SerializerInterface;
use Doctrine\Common\Collections\ArrayCollection;
use phpDocumentor\Reflection\DocBlock\Tags\Author;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\WebLink\Link;

/**
 * @Route("/channel")
 */
class ChannelController extends AbstractController
{
    /**
     * @Route("/", name="channel")
     */
    public function index(ChannelRepository $channelRepository): Response
    {
        $channels = $channelRepository->findAll();

        $channels = new ArrayCollection(
            array_merge($this->getUser()->getSentChannels()->toArray(),  $this->getUser()->getReceivedChannels()->toArray())
        );

        return $this->render('channel/index.html.twig', [
            'channels' => $channels ?? [],
        ]);
    }

    /**
     * @Route("/chat/{id}", name="chat")
     */
    public function chat(Channel $channel, MessageRepository $messageRepository, Request $request, CookieJwtProvider $cookieJwtProvider)
    {

        // dd($channel->getSender() === $this->getUser() || $channel->getReceiver() === $this->getUser());
        $this->denyAccessUnlessGranted('edit', $channel);

        $messages = $messageRepository->findBy([
            'channel' => $channel
        ], ['createdAt' => 'ASC']);

        $hubUrl = $this->getParameter('mercure.default_hub'); // Mercure automatically define this parameter
        $this->addLink($request, new Link('mercure', $hubUrl)); // Use the WebLink Component to add this header to the following response

        $response = $this->render('channel/chat.html.twig', [
            'channel' => $channel,
            'messages' => $messages
        ]);

        $response->headers->setCookie(
            Cookie::create(
                'mercureAuthorization',
                $cookieJwtProvider($channel),
                new \DateTime('+1day'),
                '/.well-known/mercure'
            )
            );
            $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }

    /**
     * @Route("/messages/{channel}", name="getMmessages")
     */
    public function messages(Channel $channel, Request $request, MessageRepository $em, SerializerInterface $serializer)
    {
        $encoder = new JsonEncoder();
        
        // $defaultContext = [
        //     AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
        //         // dd(get_class($object) === 'User');
            
        //            if(get_class($object)==="User") {
        //             return $object->getUsername();

        //            }
        //            return $object->getId();
               
        //     },
        // ];
        // $normalizer = new ObjectNormalizer(null, null, null, null, null, null, $defaultContext);

        // $serializer = new Serializer([$normalizer], [$encoder]);
        $message = $em->findLastMessage($channel);

        // dd($serializer->serialize($message, 'json'));


        // $sender = json_decode($serializer->serialize($message->getAuthor(), 'json', [
        //     'groups' => ['message'] 
        // ]),true);

        // $receiver = json_decode($serializer->serialize($message->getReceiver(), 'json', [
        //     'groups' => ['message'] 
        // ]),true);

        // $message = $serializer->serialize($message, 'json', [
        //     'groups' => ['message'] 
        // ]);
        // $message = json_decode($message, true);
         
        // $message['author'] = $sender;
        // dd($sender);
        // array_push($tempArray, $your_data);
        $response =  new StreamedResponse();
        $response->setCallback(function () use($message, $serializer) {
            
            $message = $serializer->serialize($message, 'json', [
                ObjectNormalizer::ENABLE_MAX_DEPTH => true,
                ObjectNormalizer::GROUPS =>'message' ,
            ]);

            $message = json_encode($message);


            echo 'data: ' . json_encode($message) . "\n\n";
    
            ob_flush();
            flush();
    
            usleep(200000);
 

        });
       
    
        $response->headers->set('Content-Type', 'text/event-stream');
        $response->headers->set('X-Accel-Buffering', 'no');
        $response->headers->set('Cach-Control', 'no-cache');
        $response->send();
       return $response;
    }

    /**
     * @Route("/create/{receiver}", name="createChannel")
     */
    public function create(User $receiver, Request $request,  KernelInterface $kernel)
    {

        $application = new Application($kernel);
        $application->setAutoExit(false);
        $userid = $this->getUser()->getId();

        $input = new ArrayInput([
            'command' => 'create:channel',
            'name' => $receiver->getUsername(),
            'receiver' => $receiver->getId(),
            'sender' => $userid
            
        ]);

        $output = new NullOutput();
        $application->run($input, $output);

        return $this->redirectToRoute('channel');
    }

     /**
     * @Route("/delete/{channel}", name="deleteChannel")
     */
    public function delete(Channel $channel, Request $request,  KernelInterface $kernel)
    {

        $application = new Application($kernel);
        $application->setAutoExit(false);
    

        $input = new ArrayInput([
            'command' => 'delete:channel',
            'name' => $channel->getName(),

            
        ]);

        $output = new NullOutput();
        $application->run($input, $output);

        return $this->redirectToRoute('channel');
    }
}
