<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ChannelRepository;
use App\Entity\Message;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mercure\PublisherInterface;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;


use Symfony\Component\Config\Definition\Exception\Exception;

class MessageController extends AbstractController
{
    /**
     * @Route("/message", name="message", methods={"POST"})
     */
    public function sendMessage(
        Request $request,
        ChannelRepository $channelRepository,
        SerializerInterface $serializer,
        EntityManagerInterface $em,
        HubInterface $hub) : Response
    {
        $data = \json_decode($request->getContent(), true);
        if (empty($content = $data['content'])) {
            throw new Exception('No data sent');
        }

        $channel = $channelRepository->findOneBy([
            'id' => $data['channel'] 
        ]);
        if (!$channel) {
            throw new Exception('Message have to be sent on a specific channel');
        }

        $message = new Message();
        $message->setContent($content);
        $message->setChannel($channel);
        $message->setAuthor($this->getUser());

        $em->persist($message);
        $em->flush();

        $jsonMessage = $serializer->serialize($message, 'json', [
            'groups' => ['message'] 
        ]);

        $update = new Update(
            sprintf('http://astrochat.com/channel/%s', 
            $channel->getId()),
            $jsonMessage
        );

        $hub->publish($update); 
        
        return $this->json( 
            $jsonMessage,
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/message/send", name="send", methods={"POST"})
     */
    public function sendM(
        Request $request,
        ChannelRepository $channelRepository,
        SerializerInterface $serializer,
        EntityManagerInterface $em,
        HubInterface $hub) : Response
    {
       
        $data = \json_decode($request->getContent(), true);
        if (empty($content = $data['content'])) {
            throw new Exception('No data sent');
        }

        $channel = $channelRepository->findOneBy([
            'id' => $data['channel'] 
        ]);
        if (!$channel) {
            throw new Exception('Message have to be sent on a specific channel');
        }

        $message = new Message();
        $message->setContent($content);
        $message->setChannel($channel);
        $message->setAuthor($this->getUser());
        (($channel->getSender() === $this->getUser())) ? $message->setReceiver($channel->getReceiver()) : $message->setReceiver($channel->getSender());

        $em->persist($message);
        $em->flush();

        $jsonMessage = $serializer->serialize($message, 'json', [
            'groups' => ['message'] 
        ]);

        return $this->json( 
            $jsonMessage,
            Response::HTTP_OK
        );
    }

    
}
