<?php

namespace App\Controller;

use App\Repository\CompanyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\lib\IgdbBundle\IgdbWrapper\IgdbWrapper;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TestController extends AbstractController
{
    /**
     * @Route("/test-company", name="test-company")
     */
    public function testCompany(IgdbWrapper $igdb)
    {
        $results = [];
        $companiesCount = $igdb->countCompanies()/500;
        for ($i=0; $i < $companiesCount; $i++) {  
            $offset = ($i == 0)? 0 : $i*500;
            $response = $igdb->getCompanies($offset);
            array_push($results, $response);
        }

        $response = new Response(json_encode($results));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/test-genre", name="test-genre")
     */
    public function testGenre(IgdbWrapper $igdb)
    {
        $genres = $igdb->getGenres();

        $response = new Response(json_encode($genres));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/test-platform", name="test-platorm")
     */
    public function testPlatform(IgdbWrapper $igdb)
    {
        $platforms = $igdb->getCharacters();

        $response = new Response(json_encode($platforms));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
