<?php

namespace App\Form;

use App\Entity\Answer;
use App\Entity\Question;
use App\Entity\TypeAnswer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AnswerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, ['required' => true])
            ->add('type', EntityType::class, [
                'class' => TypeAnswer::class,
                'choice_label' => 'wording',
                'required' => true
            ])
            ->add('question_from', EntityType::class, [
                'class' => Question::class,
                'choice_label' => 'title',
                'required' => true
            ])
            ->add('question_to', EntityType::class, [
                'class' => Question::class,
                'choice_label' => 'title',
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Answer::class,
        ]);
    }
}
