<?php

namespace App\Form;

use App\Entity\Game;
use App\Entity\Genre;
use App\Entity\GameMode;
use App\Entity\Platform;
use App\Entity\Company;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class FilterGameType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ->add('name', SearchType::class, [
            //     'label' => false,
            //     'required' => false,
            //     'attr' => [
            //         'placeholder' => 'Search',
            //         'class' => 'form-control me-2',
            //         'role' => 'search',
            //         'minlength' => 3
            //     ],
            // ])
            ->add('platforms', EntityType::class, [
                'label' => "Plateforme",
                'required' => false,
                'class' => Platform::class,
                'choice_label' => 'name',
                'expanded' => false,
                'multiple' => true
            ])
            ->add('genres', EntityType::class, [
                'label' => "Genre",
                'required' => false,
                'class' => Genre::class,
                'choice_label' => 'name',
                'expanded' => true,
                'multiple' => true
            ])
            ->add('modes', EntityType::class, [
                'label' => "Mode de jeu",
                'required' => false,
                'class' => GameMode::class,
                'choice_label' => 'name',
                'expanded' => true,
                'multiple' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Game::class,
        ]);
    }
}
