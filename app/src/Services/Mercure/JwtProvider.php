<?php

declare(strict_types=1);

namespace App\Services\Mercure;

use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key;

class JwtProvider
{
    private Configuration $config;
    
    public function __construct(string $secret)
    {
        $config = Configuration::forAsymmetricSigner(
            // You may use RSA or ECDSA and all their variations (256, 384, and 512) and EdDSA over Curve25519
            new Sha256(),
            InMemory::plainText($secret),
            InMemory::plainText('Swapit')
            // You may also override the JOSE encoder/decoder if needed by providing extra arguments here
        );
        
        $this->config =$config;
    }

    public function __invoke(): string
    {

        return ($this->config->builder())
            ->withClaim('mercure', ['publish' => ['*']])
            ->getToken($this->config->signer(), $this->config->signingKey())
            ->toString();
    }
}